from debexpo.model.data_store import DataStore

DATA_STORE_INIT_OBJECTS = (
    DataStore(namespace='_remove_uploads_',
              code='gmane.linux.debian.devel.changes.unstable', value='527427'),
    DataStore(namespace='_remove_uploads_',
              code='gmane.linux.debian.devel.changes.stable', value='10836'),
    DataStore(namespace='_remove_uploads_',
              code='gmane.linux.debian.backports.changes', value='37694'),
    )
