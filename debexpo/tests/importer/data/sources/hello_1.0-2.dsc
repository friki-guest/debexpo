-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: any
Version: 1.0-2
Maintainer: Baptiste BEAUPLAT <lyknode@cilg.org>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.1.3
Build-Depends: xxd, debhelper-compat (= 12)
Package-List:
 hello deb unknown optional arch=any
Checksums-Sha1:
 be505a70074ab85fed3f68026242651fcde85bf2 168 hello_1.0.orig.tar.xz
 14ec483ca941a5758c19cd26a32ec11501bd565e 1956 hello_1.0-2.debian.tar.xz
Checksums-Sha256:
 622d2165e0ae0aee8b3dac5fbf07f471db2205c4cb60d1c3ce3762a12fbe62bf 168 hello_1.0.orig.tar.xz
 343681eed53d931907fd4acc0ad3b9abea438bed6a39cc71a8fe0a0fa03c5db2 1956 hello_1.0-2.debian.tar.xz
Files:
 3789bf498b8c4cf0387383466ec4c227 168 hello_1.0.orig.tar.xz
 4c28fb46f2331c666a3a9712380f39de 1956 hello_1.0-2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iHUEARYIAB0WIQRVkwbu4cjBst0cc7HENHgc6HHz3wUCXLtolgAKCRDENHgc6HHz
3wLGAP9jhP3AasKMD3TKKMAknsoqUwjFhxTWiSWLHGj616A+uAD+OroEvYdr9+E0
zkSFrjn40D5wRZqBUMridzfwwmHbnws=
=BNft
-----END PGP SIGNATURE-----
