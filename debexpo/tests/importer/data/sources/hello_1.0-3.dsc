-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: any
Version: 1.0-3
Maintainer: Baptiste BEAUPLAT <lyknode@cilg.org>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.1.3
Build-Depends: xxd, debhelper-compat (= 12)
Package-List:
 hello deb unknown optional arch=any
Checksums-Sha1:
 be505a70074ab85fed3f68026242651fcde85bf2 168 hello_1.0.orig.tar.xz
 d76c572f2986f042ebd1132725d45f44dc804cac 544 hello_1.0-3.debian.tar.xz
Checksums-Sha256:
 622d2165e0ae0aee8b3dac5fbf07f471db2205c4cb60d1c3ce3762a12fbe62bf 168 hello_1.0.orig.tar.xz
 96d55fcd34e5a7bacd21dcc29582dae169614be437996528aa8157a03c5cfdd8 544 hello_1.0-3.debian.tar.xz
Files:
 3789bf498b8c4cf0387383466ec4c227 168 hello_1.0.orig.tar.xz
 2e2ce412b725ec86e2acf5e506902261 544 hello_1.0-3.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iHUEARYIAB0WIQRVkwbu4cjBst0cc7HENHgc6HHz3wUCXLuligAKCRDENHgc6HHz
351BAQDebXVvffGqx01xUpjNFv4FR19tOpVRVPoWzB2DLvZDVwD9EtDUUl+Jrv5Q
EmbBe6HYIW+9dM7zhfGp9SGkbgddBQ0=
=cFQu
-----END PGP SIGNATURE-----
