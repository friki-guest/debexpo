-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: any
Version: 1.0-4
Maintainer: Baptiste BEAUPLAT <lyknode@cilg.org>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.1.3
Build-Depends: xxd, debhelper-compat (= 12)
Package-List:
 hello deb unknown optional arch=any
Checksums-Sha1:
 be505a70074ab85fed3f68026242651fcde85bf2 168 hello_1.0.orig.tar.xz
 8e124385f4a53db565125a7fde7d2d0bb27440a8 620 hello_1.0-4.debian.tar.xz
Checksums-Sha256:
 622d2165e0ae0aee8b3dac5fbf07f471db2205c4cb60d1c3ce3762a12fbe62bf 168 hello_1.0.orig.tar.xz
 cea73f47c4c3c91d3afe9205f4bb668fc3700e1a86a0ffa1e1505054eccbba9c 620 hello_1.0-4.debian.tar.xz
Files:
 3789bf498b8c4cf0387383466ec4c227 168 hello_1.0.orig.tar.xz
 e5aae9a0fdd94f9b86e671a886090863 620 hello_1.0-4.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iHUEARYIAB0WIQRVkwbu4cjBst0cc7HENHgc6HHz3wUCXLulhgAKCRDENHgc6HHz
34VyAP4lQquQppzMMDCBo6K8nF5u6e43R11ml8Nd+qkSEYtILAD6An9PbVAZjYch
lQvS3MRqG/ZNDxMyscYUXeU+t+nwFgY=
=eMQU
-----END PGP SIGNATURE-----
